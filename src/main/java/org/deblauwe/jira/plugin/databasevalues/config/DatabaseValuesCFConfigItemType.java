package org.deblauwe.jira.plugin.databasevalues.config;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.manager.OptionsManager;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigItemType;
import com.atlassian.jira.issue.fields.layout.field.FieldLayoutItem;

/**
 * Created by Wim Deblauwe
 */
public class DatabaseValuesCFConfigItemType implements FieldConfigItemType
{
	private OptionsManager _optionsManager;

	public DatabaseValuesCFConfigItemType( OptionsManager optionsManager )
	{
		_optionsManager = optionsManager;
	}

	public String getDisplayName()
	{
		return "Database Values Options";
	}

	public String getViewHtml( FieldConfig fieldConfig, FieldLayoutItem fieldLayoutItem )
	{
		final Options options = _optionsManager.getOptions( fieldConfig );
		return prettyPrintOptions( options );
	}

	public String getObjectKey()
	{
		return "options";
	}

	public Object getConfigurationObject( Issue issue, FieldConfig config )
	{
		return _optionsManager.getOptions( config );
	}

	public String getBaseEditUrl()
	{
		return "EditDatabaseValuesOptionsAction!default.jspa";
	}

	private String prettyPrintOptions( Options options )
	{
		return options.toString();
	}

	public String getDisplayNameKey() {
		// TODO Auto-generated method stub
		return null;
	}
}
