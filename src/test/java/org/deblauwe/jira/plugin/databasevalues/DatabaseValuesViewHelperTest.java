package org.deblauwe.jira.plugin.databasevalues;

import com.atlassian.jira.util.I18nHelper;
import com.atlassian.velocity.DefaultVelocityManager;
import junit.framework.TestCase;
import org.deblauwe.jira.plugin.databasevalues.config.DatabaseValuesCFParameters;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Wim Deblauwe
 */
public class DatabaseValuesViewHelperTest extends TestCase
{
	private static final String EOL = System.getProperty( "line.separator" );


	public void testGetHtmlForView() throws ClassNotFoundException, SQLException
	{
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb-dvvht" );
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );

		parameters.setSqlQuery( "select id, name from customer" );
		parameters.setPrimaryKeyColumnNumber( 0 );
		parameters.setRenderingViewPattern( "{1} ({0})" );
		parameters.setRenderingEditPattern( "{1}" );
		parameters.setRenderingSearchPattern( "{1}" );

		cleanupDemoData( parameters );
		addDemoData( parameters );

		DatabaseValuesViewHelper helper = new DatabaseValuesViewHelper( parameters, new DefaultVelocityManager() );
		I18nHelper i18nHelper = createI18NHelper();
		assertEquals( "Wim (1)", helper.getHtmlForView( "1", null, i18nHelper ) );
		assertEquals( "Sofie (2)", helper.getHtmlForView( "2", null, i18nHelper ) );

	}

	public void testGetHtmlForEdit() throws ClassNotFoundException, SQLException
	{
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb-dvvht" );
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );

		parameters.setSqlQuery( "select id, name from customer" );
		parameters.setPrimaryKeyColumnNumber( 0 );
		parameters.setRenderingViewPattern( "{1} ({0})" );
		parameters.setRenderingEditPattern( "{1}" );
		parameters.setRenderingSearchPattern( "{1}" );

		cleanupDemoData( parameters );
		addDemoData( parameters );

		I18nHelper i18nHelper = createI18NHelper();

		DatabaseValuesViewHelper helper = new DatabaseValuesViewHelper( parameters, new DefaultVelocityManager() );
		assertEquals( "<select name=\"customfield_10000\">" + EOL +
				"<option value=\"\">common.words.none</option>" + EOL +
				"<option value=\"1\">Wim</option>" + EOL +
				"<option value=\"2\">Sofie</option>" + EOL +
				"<option value=\"3\">John</option>" + EOL +
				"</select>", helper.getHtmlForEdit( "customfield_10000", null, null,i18nHelper ) );
		assertEquals( "<select name=\"customfield_10000\">" + EOL +
				"<option value=\"\">common.words.none</option>" + EOL +
				"<option value=\"1\" selected=\"selected\">Wim</option>" + EOL +
				"<option value=\"2\">Sofie</option>" + EOL +
				"<option value=\"3\">John</option>" + EOL +
				"</select>", helper.getHtmlForEdit( "customfield_10000", "1", null,i18nHelper ) );
		assertEquals( "<select name=\"customfield_10000\">" + EOL +
				"<option value=\"\">common.words.none</option>" + EOL +
				"<option value=\"1\">Wim</option>" + EOL +
				"<option value=\"2\" selected=\"selected\">Sofie</option>" + EOL +
				"<option value=\"3\">John</option>" + EOL +
				"</select>", helper.getHtmlForEdit( "customfield_10000", "2", null,i18nHelper ) );

	}

	public void testGetHtmlForSearch() throws ClassNotFoundException, SQLException
	{
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb-dvvht" );
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );

		parameters.setSqlQuery( "select id, name from customer" );
		parameters.setPrimaryKeyColumnNumber( 0 );
		parameters.setRenderingViewPattern( "{1} ({0})" );
		parameters.setRenderingEditPattern( "{1}" );
		parameters.setRenderingSearchPattern( "{1}" );

		cleanupDemoData( parameters );
		addDemoData( parameters );
		I18nHelper i18nHelper = createI18NHelper();

		DatabaseValuesViewHelper helper = new DatabaseValuesViewHelper( parameters, new DefaultVelocityManager() );
		assertEquals( "<select name=\"customfield_10000\" id=\"customfield_10000\"  class=\"standardInputField\" multiple=\"multiple\">" + EOL +
				"<option value=\"-1\" selected=\"selected\">common.filters.any</option>" + EOL +
				"<option value=\"1\">Wim</option>" + EOL +
				"<option value=\"2\">Sofie</option>" + EOL +
				"<option value=\"3\">John</option>" + EOL +
				"</select>", helper.getHtmlForSearch( "customfield_10000", null, null,i18nHelper ) );
		assertEquals( "<select name=\"customfield_10000\" id=\"customfield_10000\"  class=\"standardInputField\" multiple=\"multiple\">" + EOL +
				"<option value=\"-1\" selected=\"selected\">common.filters.any</option>" + EOL +
				"<option value=\"1\">Wim</option>" + EOL +
				"<option value=\"2\">Sofie</option>" + EOL +
				"<option value=\"3\">John</option>" + EOL +
				"</select>", helper.getHtmlForSearch( "customfield_10000", new ArrayList(), null, i18nHelper ) );
		ArrayList selectedValues = new ArrayList();
		selectedValues.add( "-1" );
		assertEquals( "<select name=\"customfield_10000\" id=\"customfield_10000\"  class=\"standardInputField\" multiple=\"multiple\">" + EOL +
				"<option value=\"-1\" selected=\"selected\">common.filters.any</option>" + EOL +
				"<option value=\"1\">Wim</option>" + EOL +
				"<option value=\"2\">Sofie</option>" + EOL +
				"<option value=\"3\">John</option>" + EOL +
				"</select>", helper.getHtmlForSearch( "customfield_10000", selectedValues, null,i18nHelper ) );

		selectedValues = new ArrayList();
		selectedValues.add( "1" );
		assertEquals( "<select name=\"customfield_10000\" id=\"customfield_10000\"  class=\"standardInputField\" multiple=\"multiple\">" + EOL +
				"<option value=\"-1\">common.filters.any</option>" + EOL +
				"<option value=\"1\" selected=\"selected\">Wim</option>" + EOL +
				"<option value=\"2\">Sofie</option>" + EOL +
				"<option value=\"3\">John</option>" + EOL +
				"</select>", helper.getHtmlForSearch( "customfield_10000", selectedValues,null, i18nHelper ) );

		cleanupDemoData( parameters );
	}

	public void testCascadingSelectHtmlForEdit() throws ClassNotFoundException, SQLException
	{
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb-dvvht" );
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );

		parameters.setSqlQuery( "select id, name, country from customer" );
		parameters.setPrimaryKeyColumnNumber( 0 );
		parameters.setRenderingViewPattern( "{1} ({0})" );
		parameters.setRenderingEditPattern( "{1}" );
		parameters.setRenderingSearchPattern( "{1}" );
		parameters.setGroupingColumnNumber( 2 );

		cleanupDemoData( parameters );
		addDemoData( parameters );
		I18nHelper i18nHelper = createI18NHelper();

		DatabaseValuesViewHelper helper = new DatabaseValuesViewHelper( parameters, new DefaultVelocityManager() );
		String htmlwithNoValue = helper.getCascadingSelectHtmlForEdit( "customfield_10000", null, false, null, i18nHelper );
		assertEquals( "<select class=\"select cascadingselect-parent\" name=\"customfield_10000:1\" id=\"customfield_10000:1\">\n" +
							  "<option class=\"default-option\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"0\">Belgium</option>\n" +
							  "<option class=\"option-group-1\" value=\"1\">Australia</option>\n" +
							  "</select>\n" +
							  "<select class=\"select cascadingselect-child\" name=\"customfield_10000\" id=\"customfield_10000\">\n" +
							  "<option value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\"  value=\"1\">Wim</option>\n" +
							  "<option class=\"option-group-0\"  value=\"2\">Sofie</option>\n" +
							  "<option class=\"option-group-1\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-1\"  value=\"3\">John</option>\n" +
							  "</select>", htmlwithNoValue );

		String htmlForJohnSelected = helper.getCascadingSelectHtmlForEdit( "customfield_10000", "3", false, null,i18nHelper );
		assertEquals( "<select class=\"select cascadingselect-parent\" name=\"customfield_10000:1\" id=\"customfield_10000:1\">\n" +
							  "<option class=\"default-option\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"0\">Belgium</option>\n" +
							  "<option class=\"option-group-1\" value=\"1\" selected=\"selected\">Australia</option>\n" +
							  "</select>\n" +
							  "<select class=\"select cascadingselect-child\" name=\"customfield_10000\" id=\"customfield_10000\">\n" +
							  "<option value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\"  value=\"1\">Wim</option>\n" +
							  "<option class=\"option-group-0\"  value=\"2\">Sofie</option>\n" +
							  "<option class=\"option-group-1\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-1\"  value=\"3\" selected=\"selected\">John</option>\n" +
							  "</select>", htmlForJohnSelected );
	}

	public void testCascadingSelectHtmlForEditSorting() throws ClassNotFoundException, SQLException
	{
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb-dvvht" );
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );

		parameters.setSqlQuery( "select id, name, country from customer order by country, name" );
		parameters.setPrimaryKeyColumnNumber( 0 );
		parameters.setRenderingViewPattern( "{1} ({0})" );
		parameters.setRenderingEditPattern( "{1}" );
		parameters.setRenderingSearchPattern( "{1}" );
		parameters.setGroupingColumnNumber( 2 );

		cleanupDemoData( parameters );
		addDemoData( parameters );
		I18nHelper i18nHelper = createI18NHelper();

		DatabaseValuesViewHelper helper = new DatabaseValuesViewHelper( parameters, new DefaultVelocityManager() );
		String htmlwithNoValue = helper.getCascadingSelectHtmlForEdit( "customfield_10000", null, false, null, i18nHelper );
		assertEquals( "<select class=\"select cascadingselect-parent\" name=\"customfield_10000:1\" id=\"customfield_10000:1\">\n" +
							  "<option class=\"default-option\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"0\">Australia</option>\n" +
							  "<option class=\"option-group-1\" value=\"1\">Belgium</option>\n" +
							  "</select>\n" +
							  "<select class=\"select cascadingselect-child\" name=\"customfield_10000\" id=\"customfield_10000\">\n" +
							  "<option value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\"  value=\"3\">John</option>\n" +
							  "<option class=\"option-group-1\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-1\"  value=\"2\">Sofie</option>\n" +
							  "<option class=\"option-group-1\"  value=\"1\">Wim</option>\n" +
							  "</select>", htmlwithNoValue );

		String htmlForJohnSelected = helper.getCascadingSelectHtmlForEdit( "customfield_10000", "3", false, null,i18nHelper );
		assertEquals( "<select class=\"select cascadingselect-parent\" name=\"customfield_10000:1\" id=\"customfield_10000:1\">\n" +
							  "<option class=\"default-option\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"0\" selected=\"selected\">Australia</option>\n" +
							  "<option class=\"option-group-1\" value=\"1\">Belgium</option>\n" +
							  "</select>\n" +
							  "<select class=\"select cascadingselect-child\" name=\"customfield_10000\" id=\"customfield_10000\">\n" +
							  "<option value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"default-option\" value=\"-1\">common.words.none</option>\n" +
							  "<option class=\"option-group-0\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\"  value=\"3\" selected=\"selected\">John</option>\n" +
							  "<option class=\"option-group-1\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-1\"  value=\"2\">Sofie</option>\n" +
							  "<option class=\"option-group-1\"  value=\"1\">Wim</option>\n" +
							  "</select>", htmlForJohnSelected );
	}

	public void testCascadingSelectHtmlForEditIfRequired() throws ClassNotFoundException, SQLException
	{
		DatabaseValuesCFParameters parameters = new DatabaseValuesCFParameters();
		parameters.setDatabaseConnectionUrl( "jdbc:hsqldb:mem:plugintestdb-dvvht" );
		parameters.setDatabaseDriver( "org.hsqldb.jdbcDriver" );
		parameters.setDatabaseUser( "sa" );
		parameters.setDatabasePassword( "" );

		parameters.setSqlQuery( "select id, name, country from customer" );
		parameters.setPrimaryKeyColumnNumber( 0 );
		parameters.setRenderingViewPattern( "{1} ({0})" );
		parameters.setRenderingEditPattern( "{1}" );
		parameters.setRenderingSearchPattern( "{1}" );
		parameters.setGroupingColumnNumber( 2 );

		cleanupDemoData( parameters );
		addDemoData( parameters );
		I18nHelper i18nHelper = createI18NHelper();

		DatabaseValuesViewHelper helper = new DatabaseValuesViewHelper( parameters, new DefaultVelocityManager() );
		String htmlwithNoValue = helper.getCascadingSelectHtmlForEdit( "customfield_10000", null, true, null,i18nHelper );
		assertEquals( "<select class=\"select cascadingselect-parent\" name=\"customfield_10000:1\" id=\"customfield_10000:1\">\n" +
							  "<option class=\"default-option\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\" value=\"0\">Belgium</option>\n" +
							  "<option class=\"option-group-1\" value=\"1\">Australia</option>\n" +
							  "</select>\n" +
							  "<select class=\"select cascadingselect-child\" name=\"customfield_10000\" id=\"customfield_10000\">\n" +
							  "<option value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\"  value=\"1\">Wim</option>\n" +
							  "<option class=\"option-group-0\"  value=\"2\">Sofie</option>\n" +
							  "<option class=\"option-group-1\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-1\"  value=\"3\">John</option>\n" +
							  "</select>", htmlwithNoValue );

		String htmlForJohnSelected = helper.getCascadingSelectHtmlForEdit( "customfield_10000", "3", true, null,i18nHelper );
		assertEquals( "<select class=\"select cascadingselect-parent\" name=\"customfield_10000:1\" id=\"customfield_10000:1\">\n" +
							  "<option class=\"default-option\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\" value=\"0\">Belgium</option>\n" +
							  "<option class=\"option-group-1\" value=\"1\" selected=\"selected\">Australia</option>\n" +
							  "</select>\n" +
							  "<select class=\"select cascadingselect-child\" name=\"customfield_10000\" id=\"customfield_10000\">\n" +
							  "<option value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-0\"  value=\"1\">Wim</option>\n" +
							  "<option class=\"option-group-0\"  value=\"2\">Sofie</option>\n" +
							  "<option class=\"option-group-1\" value=\"\">common.words.pleaseselect</option>\n" +
							  "<option class=\"option-group-1\"  value=\"3\" selected=\"selected\">John</option>\n" +
							  "</select>", htmlForJohnSelected );
	}

	private void addDemoData( DatabaseValuesCFParameters parameters ) throws ClassNotFoundException, SQLException
	{
		Connection c = null;
		try
		{
			c = createConnection( parameters );

			Statement s = c.createStatement();

			s.executeUpdate( "CREATE TABLE customer ( id INTEGER IDENTITY, name VARCHAR(256), country VARCHAR(256) )" );
			s.executeUpdate( "INSERT INTO customer VALUES( 1, 'Wim', 'Belgium')" );
			s.executeUpdate( "INSERT INTO customer VALUES( 2, 'Sofie', 'Belgium')" );
			s.executeUpdate( "INSERT INTO customer VALUES( 3, 'John', 'Australia')" );
		}
		finally
		{
			if (c != null)
			{
				c.close();
			}
		}
	}

	private void cleanupDemoData( DatabaseValuesCFParameters parameters ) throws SQLException, ClassNotFoundException
	{
		Connection c = null;
		try
		{
			c = createConnection( parameters );

			Statement s = c.createStatement();

			s.executeUpdate( "DROP TABLE customer IF EXISTS" );
		}
		finally
		{
			if (c != null)
			{
				c.close();
			}
		}
	}

	private Connection createConnection( DatabaseValuesCFParameters parameters )
			throws SQLException, ClassNotFoundException
	{
		Class.forName( parameters.getDatabaseDriver() );
		return DriverManager.getConnection( parameters.getDatabaseConnectionUrl(),
											parameters.getDatabaseUser(),
											parameters.getDatabasePassword() );
	}

	private I18nHelper createI18NHelper()
	{
		I18nHelper i18nHelper = mock( I18nHelper.class );
		when( i18nHelper.getText( anyString() ) ).then(returnsFirstArg() );
		return i18nHelper;
	}

}
